from core_analyzer import getCoreType
aliasesList = []
def load_aliases():
    from core import log
    import csv
    with open('config/aliases.csv', 'rt', encoding="Windows-1251") as fin:
        cin = csv.DictReader(fin)
        csvdict = [row for row in cin]
        for item in csvdict:
            aliasesList.append({'text':item['text'], 'command':item['command'], 'module':item['module'], 'peer':item['peer']})  
        log(0, "{} aliases loaded.".format(len(csvdict)))            
def save_aliases():
    from core import log
    import csv
    csvrecord = []
    for alias in aliasesList:
        csvrecord.append({'text':alias['text'], 'command':alias['command'], 'module':alias['module'], 'peer':alias['peer']})
    with open('config/aliases.csv', 'wt', newline='', encoding="Windows-1251") as fout:
        cout = csv.DictWriter(fout, ['text', 'command', 'module', 'peer'])
        cout.writeheader()
        cout.writerows(csvrecord) 
def addAlias(msg,chat_id):
    import formatter
    resTuple = formatter.format('ALIAS_ADD', msg)
    if not resTuple:
        return False
    alias_text, module, command = resTuple
    for alias in aliasesList:
        if alias['text'] == alias_text and alias['peer'] == str(chat_id):
            return False
    aliasesList.append({'text':alias_text, 'module':module, 'command':command, 'peer':chat_id})
    save_aliases()
    return True
def rmAlias(name, chat_id):
    index = 0
    for alias in aliasesList:
        if alias['text'] == name and alias['peer'] == str(chat_id):
            save_aliases()
            load_aliases()
            return True
        index += 1
    return False
def getAliasCmd(msg, chat_id):
    import formatter
    import re
    from core_analyzer import CORE_KEYWORDS
    for alias in aliasesList:
        if re.findall(alias['text'], msg) and int(alias['peer'])==chat_id:
            args = formatter.format('ALIAS_USE', msg.replace(alias['text'], ''))
            return (alias['module'], alias['command'], args)
    return False

def isAlias(msg):
    from core_analyzer import CORE_KEYWORDS
    from re import findall, compile
    for alias in aliasesList:
        template = compile('%s%s' % (CORE_KEYWORDS['BOT_USE'], alias['text']))
        if findall(template, msg):
            return True