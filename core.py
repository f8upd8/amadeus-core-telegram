config = {}
moduleList = dict()
chats = {}

def log(logType, msg):
    import time
    import datetime
    import os
    import platform
    if platform.system() == "Windows":
        filepath = 'logs/'+datetime.date.today().isoformat()+'.log'
    elif platform.system() == "Linux":
        filepath = 'logs\\\\'+datetime.date.today().isoformat()+'.log'
    if logType == 0:
        msg = '[{}]: {}: {}'.format(time.ctime(time.time()), 'SYSTEM', msg)
    elif logType == 1:
        msg = '[{}]: {}: {}'.format(time.ctime(time.time()), 'ERROR', msg)
    print(msg)
    try:
        file = open(filepath, 'xt')
    except OSError:
        file = open(filepath, 'at')
    print(msg, file=file)
    file.close()

def load_cfg(path='config/config.cfg'):
    import configparser
    import os
    global config
    import core_analyzer
    import shutil
    cfg = configparser.ConfigParser()
    cfgfile = cfg.read(path)
    if not cfgfile:
        shutil.copyfile('config/default.cfg', path)
        cfgfile = cfg.read(path)
    config = {'TOKEN':cfg['api']['token'],
              'POLL_TIMEOUT':cfg['longpoll']['timeout'],
              'DEFAULT_LANGUAGE':cfg['system']['language'],
              'SENPAI_ID':cfg['system']['senpai_id']}
    core_analyzer.load_lang(config['DEFAULT_LANGUAGE'])
    log(0, "Config loaded successfully")

def module_load(name):
    import imp
    module = False
    try:
        fp, pathname, description = imp.find_module(name, path=['./modules'])
        module = imp.load_module(name,fp, pathname, description)
    except Exception as exp:
        log(1, str(exp))
        return False
    if module:
        moduleList[name] = module
        log(0, 'Module %s loaded successfully.' % name)
        return True    

def module_unload(name):
    if moduleList.get(name):
        del(moduleList[name])
        log(0, 'Module %s was unloaded' % name)
    else:
        log(1, "No module named '{}'".format(name))
    
def save_chats():
    import csv
    csvrecord = []
    for chat, state in chats.items():
        csvrecord.append({'chat':chat, 'state':state})
    with open('config/chats.csv', 'wt', newline='') as fout:
        cout = csv.DictWriter(fout, ['chat', 'state'])
        cout.writeheader()
        cout.writerows(csvrecord)

def load_chats():
    import csv
    with open('config/chats.csv', 'rt') as fin:
        cin = csv.DictReader(fin)
        csvdict = [row for row in cin]
        for item in csvdict:
            chats[int(item['chat'])] = item['state']
  
def TGApiQuery(method, **kwargs):
    import json
    import requests
    return json.loads(requests.post('https://api.telegram.org/bot{token}/{method}?'.format(token=config['TOKEN'], method=method), kwargs).text)

def IssueCommand(module, command, chat_id, user_id, msg_id, arguments):
    args = (chat_id, user_id, msg_id, arguments)
    if not moduleList.get(module):
        return ('NO_MODULE', '')
    try:
        callback = moduleList.get(module).invoke(command, args)
    except:
        return False
    return callback
def sendBack(msgtype, msgtext, peer_id, msg_id=-1):
    if msgtype == 0:
        if msg_id == -1:
            TGApiQuery('sendMessage', text=msgtext, chat_id=peer_id)
        else:
            TGApiQuery('sendMessage', text=msgtext, chat_id=peer_id, reply_to_message_id=msg_id)

def TGInitLongPoll():
    UPTIME = 0
    import time
    import formatter
    import reactions
    #import sys
    #non_bmp_map = dict.fromkeys(range(0x10000, sys.maxunicode + 1), 0xfffd)
    STARTDATE = time.time()
    import json
    from core_analyzer import CORE_KEYWORDS, CORE_REPLIES, getCoreType
    log(0, 'Initiated Telegram Long Polling')
    offset = 0
    while True:
        #Getting updates
        response = TGApiQuery('getUpdates', offset = offset, timeout = config['POLL_TIMEOUT'])
        if not response or not response.get('result'):
            continue
        for update in response.get('result'):
            #updmsg = json.dumps(update).translate(non_bmp_map)
            offset = update.get('update_id') + 1
            #Grabbing essential information for processing
            msg = update.get('message')
            if not msg:
                continue
            msg = msg.get('text')
            if not msg:
                continue     
            user_id = update['message'].get('from').get('id')
            msg_id = update['message'].get('message_id')
            chat_id = update['message']['chat'].get('id')
            chat_type = update['message']['chat'].get('type')
            #Grabbing command type
            CORE_TYPE, msg = getCoreType(msg)
            #Checking if chat is enabled
            if not (CORE_TYPE == 'CHAT_ENABLE' or CORE_TYPE == 'CHAT_DISABLE') and (chat_type != 'private' and chats.get(chat_id) != 'enabled'):
                if CORE_TYPE != "UNKNOWN_COMMAND":
                    TGApiQuery('sendMessage', chat_id = user_id, text=CORE_REPLIES['CHAT_DISABLED'])
                continue
            if not msg.startswith(CORE_KEYWORDS['BOT_USE']) or (CORE_TYPE == "UNKNOWN_COMMAND"):
                response = reactions.checkForReaction(msg)
                if not response:
                    continue
                method, kwargs = response
                #print(response)
                if method == "sendMessage":
                    TGApiQuery("sendChatAction", chat_id = chat_id, action="typing")
                    time.sleep(1)
                elif method == "sendSticker":
                    TGApiQuery("sendChatAction", chat_id = chat_id, action="upload_photo")
                    time.sleep(1)
                TGApiQuery(method, chat_id = chat_id, reply_to_message_id=msg_id, **kwargs)
                continue
            #Performing user-allowed commands:
            if CORE_TYPE == 'ISSUE_COMMAND':
                module, command, args = formatter.format(CORE_TYPE, msg)
                method, kwargs = IssueCommand(module, command, chat_id, user_id, msg_id, args)
                if method == 'NO_MODULE':
                    sendBack(0, CORE_REPLIES['NO_MODULE'], chat_id, msg_id)
                elif method == 'NO_COMMAND':
                    sendBack(0, CORE_REPLIES['NO_COMMAND'], chat_id, msg_id)
                else:
                    TGApiQuery(method, **kwargs)
            elif CORE_TYPE == 'UNKNOWN_COMMAND':
                pass
                #sendBack(0, CORE_REPLIES['UNKNOWN_MESSAGE'], chat_id, msg_id)
            elif CORE_TYPE == 'ALIAS_ADD':
                import aliases
                if aliases.addAlias(msg, chat_id):
                    sendBack(0, CORE_REPLIES['ALIAS_ADDED'], chat_id, msg_id)
                else:
                    sendBack(0, CORE_REPLIES['ALIAS_ADD_FAILED'], chat_id, msg_id)
            elif CORE_TYPE == 'ALIAS_USE':
                import aliases
                resTuple = aliases.getAliasCmd(msg, chat_id)
                if not resTuple:
                    sendBack(0, CORE_REPLIES['UNKNOWN_MESSAGE'], chat_id, msg_id)
                    continue
                module, command, arguments = resTuple
                method, kwargs = IssueCommand(module, command, chat_id, user_id, msg_id, arguments)
                if method == 'NO_MODULE':
                    sendBack(0, CORE_REPLIES['NO_MODULE'], chat_id, msg_id)
                elif method == 'NO_COMMAND':
                    sendBack(0, CORE_REPLIES['NO_COMMAND'], chat_id, msg_id)
                else:
                    TGApiQuery(method, **kwargs)
            elif CORE_TYPE == 'ALIAS_REMOVE':
                import aliases
                alias_text = formatter.format('ALIAS_REMOVE', msg)
                if aliases.rmAlias(alias_text, chat_id):
                    sendBack(0, CORE_REPLIES["ALIAS_REMOVED"], chat_id, msg_id)
                else:
                    sendBack(0, CORE_REPLIES["ALIAS_REMOVE_FAILED"], chat_id, msg_id)
            else:
                if config['SENPAI_ID'] != str(user_id) and not config['SENPAI_ID'] == '-1':
                    sendBack(0, CORE_REPLIES["NOT_SENPAI"], user_id, msg_id)
                    continue
            #Performing senpai-only commands:
            if CORE_TYPE == 'LANG_CHANGE':
                import core_analyzer
                lang = formatter.format('LANG_CHANGE', msg)
                if core_analyzer.load_lang(lang):
                    sendBack(0, CORE_REPLIES["LANGUAGE_CHANGED"], chat_id, msg_id)
                else:
                    sendBack(0, CORE_REPLIES["NO_LANGUAGE"], chat_id, msg_id)
            elif CORE_TYPE == 'CHAT_ENABLE':
                if chats.get(chat_id) == 'enabled':
                    continue
                elif chat_type == 'private':
                    sendBack(0, CORE_REPLIES["CHAT_IS_PRIVATE"], chat_id, msg_id)
                    continue
                chats[chat_id] = 'enabled'
                save_chats()
                sendBack(0, CORE_REPLIES["CHAT_ENABLE"], chat_id, msg_id)
            elif CORE_TYPE == 'CHAT_DISABLE':
                if chats.get(chat_id) == 'disabled':
                    continue
                elif chat_type == 'private':
                    sendBack(0, CORE_REPLIES["CHAT_IS_PRIVATE"], chat_id, msg_id)
                    continue
                chats[chat_id] = 'disabled'
                save_chats()
                sendBack(0, CORE_REPLIES["CHAT_DISABLE"], chat_id, msg_id)               
            elif CORE_TYPE == 'SENPAI_SET':
                import configparser
                import os
                cfg = configparser.ConfigParser()
                cfg.read('config/config.cfg')
                cfg['system']['senpai_id'] = str(user_id)
                with open('config/config.cfg', 'w') as configfile:
                    cfg.write(configfile)
                    TGApiQuery('sendMessage', chat_id=chat_id, text=CORE_REPLIES['NEW_SENPAI'])
                log(0, 'NEW SENPAI WITH ID {} HAS BEEN SET'.format(user_id))
                load_cfg()
            elif CORE_TYPE == "MODULE_LOAD":
                module = formatter.format('MODULE_LOAD', msg)
                if module_load(module):
                    sendBack(0, CORE_REPLIES["MODULE_LOADED"], chat_id, msg_id)
                else:
                    sendBack(0, CORE_REPLIES["MODULE_LOAD_FAILED"], chat_id, msg_id)
            elif CORE_TYPE == "UPTIME":
                TGApiQuery('sendMessage', chat_id=chat_id, text="%s %d %s" % (CORE_REPLIES['UPTIME_IS'], int(time.time()-STARTDATE), CORE_REPLIES["SECONDS"]))

def __init__():
    import aliases
    load_cfg()
    load_chats()
    aliases.load_aliases()
    TGInitLongPoll()