CORE_TEMPLATES = {}
CORE_KEYWORDS = {}
CORE_REPLIES = {}
def load_templates():
    global CORE_TEMPLATES
    from re import findall, compile
    CORE_TEMPLATES = {'ISSUE_COMMAND':compile('^%s%s .* %s .*' % (CORE_KEYWORDS['BOT_USE'], CORE_KEYWORDS['FROM'], CORE_KEYWORDS['ISSUE_COMMAND'])),
                      'LANG_CHANGE':compile('^%s%s \w*' % (CORE_KEYWORDS['BOT_USE'], CORE_KEYWORDS['LANG_CHANGE'])),
                      'MODULE_LOAD':compile('^%s%s \w*' % (CORE_KEYWORDS['BOT_USE'], CORE_KEYWORDS['MODULE_LOAD'])),
                      'CHAT_ENABLE':compile('^%s%s' % (CORE_KEYWORDS['BOT_USE'], CORE_KEYWORDS['CHAT_ENABLE'])),
                      'CHAT_DISABLE':compile('^%s%s' % (CORE_KEYWORDS['BOT_USE'], CORE_KEYWORDS['CHAT_DISABLE'])),
                      'ALIAS_ADD':compile('^%s%s ".*" %s ".*"' % (CORE_KEYWORDS['BOT_USE'], CORE_KEYWORDS['ALIAS_ADD'], CORE_KEYWORDS['WITH_ARGS'])),
                      'ALIAS_REMOVE':compile('^%s%s ".*"' % (CORE_KEYWORDS['BOT_USE'], CORE_KEYWORDS['ALIAS_REMOVE'])),
                      'SENPAI_SET':compile('^%s%s' % (CORE_KEYWORDS['BOT_USE'], CORE_KEYWORDS['SENPAI_SET'])),
                      'UPTIME':compile('^%s%s' % (CORE_KEYWORDS['BOT_USE'], CORE_KEYWORDS['UPTIME']))}
def getCoreType(msg):
    from re import findall, compile
    from aliases import isAlias
    for _type, template in CORE_TEMPLATES.items():
        results = findall(template, msg)
        if results:
            return (_type, results[0])
    if isAlias(msg):
      return ('ALIAS_USE', msg)
    return ('UNKNOWN_COMMAND', msg) 

def load_lang(lang):
    import os
    import csv
    import core
    is_succesful = True;
    if os.path.exists('lang/core/%s_keywords.lang' % lang):
        with open('lang/core/%s_keywords.lang' % lang, 'rt', newline='', encoding="Windows-1251") as fin:
            cin = csv.reader(fin)
            items = [word for word in cin]
            for item in items:
                CORE_KEYWORDS[item[0]] = item[1]
    else:
        is_succesful = False
    if os.path.exists('lang/core/%s_replies.lang' % lang):
        with open('lang/core/%s_replies.lang' % lang, 'rt', newline='', encoding="Windows-1251") as fin:
            cin = csv.reader(fin)
            items = [word for word in cin]
            for item in items:
                CORE_REPLIES[item[0]] = item[1]
    else:
        is_succesful = False
    if is_succesful:
        load_templates()
        core.log(0, "Language {} loaded successfully".format(lang))
    return is_succesful
