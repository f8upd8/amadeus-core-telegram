from core_analyzer import CORE_KEYWORDS
def format(cmdtype, msg):
	import core_analyzer
	global CORE_KEYWORDS
	msg = msg.replace(CORE_KEYWORDS['BOT_USE'], '')
	from core_analyzer import CORE_KEYWORDS
	if cmdtype == 'LANG_CHANGE':
		intermediate = msg.replace(CORE_KEYWORDS['LANG_CHANGE'], '')
		return intermediate.strip(' ')
	elif cmdtype == 'ISSUE_COMMAND':
		intermediate = msg.split(CORE_KEYWORDS['ISSUE_COMMAND'])
		module = intermediate[0].replace(CORE_KEYWORDS['FROM'], '').strip(' ')
		intermediate = intermediate[1].split(CORE_KEYWORDS['WITH_ARGS'])
		command = intermediate[0].split(' ')[1]
		if len(intermediate) > 1:
			args = intermediate[1].strip(' ').split(' ')
		else:
			args = []
		return (module, command, args)
	elif cmdtype == 'ALIAS_ADD':
		intermediate = msg.split('"')
		alias_text = intermediate[1]
		if core_analyzer.getCoreType(CORE_KEYWORDS['BOT_USE']+intermediate[3])[0] == 'ISSUE_COMMAND':
			module, command, ___ = format('ISSUE_COMMAND', intermediate[3])
		else:
			return False
		return (alias_text, module, command)
	elif cmdtype == 'ALIAS_REMOVE':
		return msg.split('"')[1]
	elif cmdtype == 'ALIAS_USE':
		return msg.strip(' ').split(' ')
	elif cmdtype == 'MODULE_LOAD':
		intermediate = msg.replace(CORE_KEYWORDS['MODULE_LOAD'], '')
		return intermediate.strip(' ')		
		


