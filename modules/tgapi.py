#IMPLEMENTATION#
def returnMyID(args):
	chat_id, user_id, msg_id, arguments = args
	return ('sendMessage', {'chat_id':chat_id, 'text':"Твой айди: "+str(user_id), 'reply_to_message_id':msg_id})
def echo(args):
	chat_id, user_id, msg_id, arguments = args
	return ('sendMessage', {'chat_id':chat_id, 'text':"Ты послал эти аргументы: "+str(arguments), 'reply_to_message_id':msg_id})
#INTERFACE#
commands = {'мой_айди':returnMyID, 'эхо':echo}
def invoke(command, args):
    if not commands.get(command):
        return ('NO_COMMAND', None)
    return commands[command](args)
def about():
    return """
Это модуль для прямого использования функций API ВКонтакте.
Доступные команды:
мой_айди [без аргументов] - возвращает ваш айди
"""
