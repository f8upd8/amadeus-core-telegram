reactions = []
responses = []
def triggers_load():
	global reactions
	with open('config/reactions_triggers', 'rt', encoding='Windows 1251') as file:
		curop = 'NONE'
		curid = -1
		while True:
			cur = file.readline()
			cur = cur[:len(cur)-1] 
			if not cur:
				break
			if cur == '#ID':
				curop = 'NEW'
				continue
			elif cur == '#KEYWORDS':
				curop = 'ADDKEY'
				continue
			elif cur == '#PHRASES':
				curop = 'ADDPHRASE'
				continue
			if curop == 'NEW':
				curid = int(cur)
				reactions.append({'id':curid, 'keywords':list(), 'phrases':list()})
			elif curop == 'ADDKEY':
				reactions[curid]['keywords'].append(cur.lower())
			elif curop == 'ADDPHRASE':
				reactions[curid]['phrases'].append(cur.lower())

#reactions container file writer
def triggers_save():
	pass #not really planned to be implemented

def responses_load():
	global reactions
	with open('config/reactions_responses', 'rt', encoding='Windows 1251') as file:
		curop = 'NONE'
		curid = -1
		while True:
			cur = file.readline()
			cur = cur[:len(cur)-1]
			if not cur:
				break
			if cur == '#ID':
				curop = 'NEW'
				continue
			elif cur == '#MESSAGES':
				curop = 'ADDMSG'
				continue
			elif cur == '#STICKERS':
				curop = 'ADDSTICK'
				continue
			elif cur == '#COOLDOWN':
				curop = 'SETCOOLDOWN'
				continue
			if curop == 'NEW':
				curid = int(cur)
				responses.append({'id':curid, 'messages':list(), 'stickers':list(), 'react_counter':0, 'cooldown':3600, 'wait_until':-1})
			elif curop == 'ADDMSG':
				responses[curid]['messages'].append(cur)
			elif curop == 'ADDSTICK':
				responses[curid]['stickers'].append(cur)
			elif curop == 'SETCOOLDOWN':
				responses[curid]['cooldown'] = int(cur)



def getResponse(r_id):
	import time
	import random
	REACTION_CAP = 3
	STICKER_CHANCE = 0.55
	if responses[r_id]['wait_until'] > time.time():
		return False
	responses[r_id]['react_counter'] += 1
	if responses[r_id]['react_counter'] == REACTION_CAP:
		responses[r_id]['wait_until'] = time.time() + responses[r_id]['cooldown']
		responses[r_id]['react_counter'] = 0
	if len(responses[r_id]['messages']) == 0 and (len(responses[r_id]['stickers']) != 0 and random.random() > STICKER_CHANCE):
		return {'type':'sticker', 'fileid':random.choice(responses[r_id]['stickers'])}
	elif len(responses[r_id]['messages']) != 0:
		return {'type':'message', 'text':random.choice(responses[r_id]['messages'])}
	else:
		return False

def checkForReaction(msg):
	import re
	msg = msg.lower()
	for reaction in reactions:
		for keyword in reaction['keywords']:
			if re.findall(keyword, msg):
				resp = getResponse(reaction['id'])
				if not resp:
					continue
				if resp['type'] == 'sticker':
					return ('sendSticker', {'sticker':resp['fileid']})
				elif resp['type'] == 'message':
					return ('sendMessage', {'text':resp['text']})
		sentence_delimiters = ['!', '?', ';']
		word_delimiters = ['(', ')', ',', '-', '>', '<', '=']
		for delimiter in sentence_delimiters:
			msg = msg.replace(delimiter, '.')
		sentences = msg.split('.')
		for sentence in sentences:
			for delimiter in word_delimiters:
				sentence = sentence.replace(delimiter, ' ')
			words = set(sentence.split(' '))
			for reaction in reactions:
				phrases = set(reaction['phrases'])
				for phrase in phrases:	
					phrase = set(phrase.split(' '))				
					if  ((words & phrase) == phrase) and (phrase != set()):
						resp = getResponse(reaction['id'])
						if not resp:
							continue
						if resp['type'] == 'sticker':
							return ('sendSticker', {'sticker':resp['fileid']})
						elif resp['type'] == 'message':
							return ('sendMessage', {'text':resp['text']})	
	return False

responses_load()
triggers_load()


