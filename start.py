import core
import time

ERRLIMIT = 25

ERRC = 0
while ERRC <= ERRLIMIT:
	try:
		core.log(0, 'INIT')
		core.__init__()
	except KeyboardInterrupt:
		break
	except Exception as ex:
		ERRC += 1
		core.log(0, 'CRITICAL ERROR OCCURED:')
		core.log(0, ex)
		core.log(0, 'RESTARTING AMADEUS SYSTEM.')
		time.sleep(5)

core.log(0, 'EXITING LOOP, TOO MANY ERRORS')
